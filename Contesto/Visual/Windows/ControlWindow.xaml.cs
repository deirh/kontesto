﻿using System.Windows;
using Contesto.Logic;
using Contesto.Visual.Pages;

namespace Contesto.Visual.Windows
{
	public partial class ControlWindow : Window
	{
		public ControlWindow()
		{
			this.InitializeComponent();

            this.Presentation.Parent = this;
		}

        private void TogglePresenterClick(object sender, RoutedEventArgs e)
        {
            Globals.Overseer.TogglePresenterVisibility();
        }

        private void AddPointsButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Globals.GameState.AddPointsRequested();

            Globals.Overseer.Presentations.Multicast( p => p.AnimatePointsAdded() );
        }

        private void ScreenSelectionChecked(object sender, System.Windows.RoutedEventArgs e)
        {
        	Globals.Overseer.ChangeScreen(Globals.GameState.CurrentScreen);
        }

	    private void BriefcaseNextWordClick(object sender, RoutedEventArgs e)
	    {
	        BriefcasePage.Logic.NextWord();
	    }

        private void BriefcaseSkipWordClick(object sender, RoutedEventArgs e)
        {
            BriefcasePage.Logic.SkipWord();
        }

	    private void BriefcaseStartStopClick(object sender, RoutedEventArgs e)
	    {
	        if (BriefcasePage.Logic.IsRunning)
	        {
	            BriefcasePage.Logic.StopCountdown();
	        }
	        else
	        {
                BriefcasePage.Logic.ResetCountdown();
                BriefcasePage.Logic.StartCountdown();
	        }
	    }

	}
}