﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Contesto.Visual.Windows
{
    public partial class PresenterWindow : Window
    {
        public PresenterWindow()
        {
            InitializeComponent();

            this.Presentation.Parent = this;

            this.StateChanged += OnStateChanged;
            this.KeyDown += OnKeyDown;
            this.Closing += OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs args)
        {
            args.Cancel = true;
            this.Hide();
        }

        #region Style events

        private void OnKeyDown(object sender, KeyEventArgs args)
        {
            if (Keyboard.IsKeyDown(Key.Enter) && (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt)))
                this.ToggleFullscreen();
        }

        private void OnStateChanged(object sender, EventArgs eventArgs)
        {
            if (this.WindowState == WindowState.Maximized)
                this.ToFullscreen(true);
            else if (this.WindowState == WindowState.Normal)
                this.ToResizable();
        }

        #endregion
    }
}
