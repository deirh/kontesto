﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Contesto.Logic;

namespace Contesto.Visual.Pages
{
	public partial class RiskPage : UserControl, IPresentationPage
	{
        public GameScreen PageName { get { return GameScreen.Risk; } }
        public ListView PlayersView { get { return this.Players; } }

        public PresentationControl Presenter { get; set; }

        public RiskPage()
		{
			this.InitializeComponent();
		}

	    public void DisplayQuestion(Question question)
	    {
            if (question == null)
                this.HideQuestion();
            else
            {
                FrameworkElement element = this.RiskPanel.ItemContainerGenerator.ContainerFromItem(question) as FrameworkElement;
                if (element == null)
                    return;

                double extraMargin = 6;
                Point topLeft = element.TranslatePoint(new Point(0 + extraMargin, 0), this.Fullscreen);
                Point bottomRight = element.TranslatePoint(new Point(element.ActualWidth - extraMargin, element.ActualHeight), this.Fullscreen);

                //this.InnerScaler.Width = element.ActualWidth;
                //this.InnerScaler.Height = element.ActualHeight;
                this.FullscreenPresenter.Margin = new Thickness(topLeft.X, topLeft.Y, this.Fullscreen.ActualWidth - bottomRight.X, this.Fullscreen.ActualHeight - bottomRight.Y);

                this.BeginStoryboard("FadeInPresenterView");
                this.BeginStoryboard("QuestionToFullscreen");
            }
	    }

	    public void HideQuestion()
	    {
            this.BeginStoryboard("FadeOutPresenterView");
            this.ResetStoryboard("QuestionToFullscreen");
	    }

        #region Events

        private void RiskQuestionMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            if (element != null)
            {
                var question = element.DataContext as Question;
                if (question != null)
                {
                    Globals.GameState.QuestionSelected = question;
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        question.IsExpended = true;
                        Globals.GameState.AddPointsRequest.Amount = Globals.GameState.QuestionSelected.Value;
                        Globals.Overseer.Presentations.Multicast(pr => pr.RiskPage.DisplayQuestion(Globals.GameState.QuestionSelected));
                    }
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        question.IsExpended = !question.IsExpended;
                    }
                }
            }
        }

        private void PresenterViewboxMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Globals.GameState.QuestionSelected = null;
            Globals.Overseer.Presentations.Multicast(pr => pr.RiskPage.HideQuestion());
        }

        #endregion

        #region Private implementations

        
        #endregion
    }
}