﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Contesto.Logic;

namespace Contesto.Visual.Pages
{
    public partial class BriefcasePage : UserControl, IPresentationPage
    {
        public GameScreen PageName { get { return GameScreen.Briefcase; } }
        public ListView PlayersView { get { return this.Players; } }

        public static BriefcasePageLogic Logic = new BriefcasePageLogic();

        public PresentationControl Presenter { get; set; }

        private Storyboard animateTimer { get { return ((Storyboard) this.FindResource("AnimateTimer")); }}

        public BriefcasePage()
        {
            this.InitializeComponent();
            Logic.RegisterPresenter(this);
        }

        internal void OnResetCountdown()
        {
            this.arc.EndAngle = 0;
        }

        internal void OnStartCountdown()
        {
            this.animateTimer.Begin(this, true);
        }

        internal void OnStopCountdown()
        {
            // Stupid workaround, wth is this bug?
            var angle = this.arc.EndAngle;
            this.animateTimer.Stop(this);
            this.arc.EndAngle = angle;
        }

        #region Events

        #endregion

        #region Private implementations

        #endregion
    }

    public class BriefcasePageLogic
    {
        public bool IsRunning { get; set; }
        public DispatcherTimer Countdown { get { return this.countdownTimer; }}

        private List<BriefcasePage> presenters = new List<BriefcasePage>();
        private DispatcherTimer countdownTimer;
        private TimeSpan tickInterval = TimeSpan.FromSeconds(1);

        public BriefcasePageLogic()
        {
            countdownTimer = new DispatcherTimer(tickInterval, DispatcherPriority.Normal, CountdownTimerTick, Application.Current.Dispatcher);
            countdownTimer.Stop();
        }
        public void RegisterPresenter(BriefcasePage page)
        {
            this.presenters.Add(page);
        }

        public void ResetCountdown()
        {
            Globals.GameState.WordTimeRemaining = TimeSpan.FromSeconds(Globals.GameState.WordTimeMaximumSeconds);
            Globals.GameState.WordCombo = 0;
            this.presenters.Multicast(p => p.OnResetCountdown());
        }

        public void StartCountdown()
        {
            this.Countdown.Start();
            this.IsRunning = true;
            this.presenters.Multicast(p => p.OnStartCountdown());
        }

        public void StopCountdown()
        {
            this.Countdown.Stop();
            this.IsRunning = false;
            this.presenters.Multicast(p => p.OnStopCountdown());
        }

        public void NextWord()
        {
            SkipWord();
            if (this.IsRunning)
                ++Globals.GameState.WordCombo;
        }

        public void SkipWord()
        {
            if (!Globals.GameState.WordEnumerator.MoveNext())
            {
                Globals.GameState.WordEnumerator = Globals.GameState.Words.GetEnumerator();
                Globals.GameState.WordEnumerator.MoveNext();
            }
            Globals.GameState.WordSelected = Globals.GameState.WordEnumerator.Current;
        }

        #region Private implementations

        private void CountdownTimerTick(object sender, EventArgs eventArgs)
        {
            Globals.GameState.WordTimeRemaining -= tickInterval;
            if (Globals.GameState.WordTimeRemaining < TimeSpan.Zero)
            {
                Globals.GameState.WordTimeRemaining = TimeSpan.Zero;
                StopCountdown();
            }
        }

        #endregion

    }
}