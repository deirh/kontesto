﻿using System.Windows.Controls;
using Contesto.Logic;

namespace Contesto.Visual.Pages
{
    public partial class ResultsPage : UserControl, IPresentationPage
    {
        public GameScreen PageName { get { return GameScreen.Results; } }
        public ListView PlayersView { get { return this.Players; } }

        public PresentationControl Presenter { get; set; }

        public ResultsPage()
        {
            this.InitializeComponent();
        }
    }
}