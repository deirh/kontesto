﻿using System.Windows;
using Contesto.Logic;
using Contesto.Visual.Windows;

namespace Contesto
{
    public partial class App : Application
    {
        public App()
        {
            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Globals.Overseer = new Overseer(new ControlWindow(), new PresenterWindow());

            this.ShutdownMode = ShutdownMode.OnMainWindowClose;
            this.MainWindow = Globals.Overseer.ControlWindow;

            Globals.GameState = GameState.Load(Globals.DataFilePath);

            Globals.Overseer.Begin();

        }

        protected override void OnExit(ExitEventArgs e)
        {
            Globals.GameState.Save(Globals.DataFilePath);

            base.OnExit(e);
        }
    }
}
