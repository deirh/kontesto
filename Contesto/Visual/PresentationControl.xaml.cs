﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Contesto.Logic;

namespace Contesto.Visual
{
	public partial class PresentationControl : UserControl, IPresentation
	{
        public new Window Parent { get; set; }
        public Overseer Overseer { get; set; }
        public IPresentationPage CurrentPage { get; private set; }

        public PresentationControl()
        {
            this.InitializeComponent();

            this.RiskPage.Presenter = this;

            this.CurrentPage = this.RiskPage;
        }

	    public IEnumerable<IPresentationPage> Pages
	    {
	        get
	        {
	            yield return this.RiskPage;
	            yield return this.BriefcasePage;
	            yield return this.ResultPage;
	        }
	    }

        public void SwitchScreen(GameScreen pageName)
        {
            switch (pageName)
            {
                case GameScreen.Risk:
                    VisualStateManager.GoToState(this, "RiskActive", true);
                    break;
                case GameScreen.Briefcase:
                    VisualStateManager.GoToState(this, "CaseActive", true);
                    break;
                case GameScreen.Results:
                    VisualStateManager.GoToState(this, "ResultsActive", true);
                    break;
            }
            this.CurrentPage = Pages.FirstOrDefault(p => p.PageName == pageName);
        }

	    public void AnimatePointsAdded()
	    {
	        if (this.CurrentPage == null || this.CurrentPage.PlayersView == null)
	            return;

	        ListViewItem playerElement = this.CurrentPage.PlayersView.ItemContainerGenerator.ContainerFromItem(Globals.GameState.AddPointsRequest.Player) as ListViewItem;
	        if (playerElement == null)
	            return;

	        var scoreControl = VisualTreeEx.EnumerateDescendants(playerElement).FirstOrDefault(d => d.Name == "Score");
	        if (scoreControl == null)
	            return;

	        var targetLocation = scoreControl.TranslatePoint(new Point(scoreControl.ActualWidth / 2, scoreControl.ActualHeight), this.LayoutRoot);
	        double intendedWidth = 200;
	        TextBlock block = new TextBlock
	                          {
	                              Margin = new Thickness(targetLocation.X - intendedWidth / 2, targetLocation.Y - 5, 0, 0),
	                              Width = intendedWidth,
                                  HorizontalAlignment = HorizontalAlignment.Left,
                                  VerticalAlignment = VerticalAlignment.Top,
	                              Text = (Globals.GameState.AddPointsRequest.Amount > 0) ? ("+" + Globals.GameState.AddPointsRequest.Amount) : Globals.GameState.AddPointsRequest.Amount.ToString(),
	                              Opacity = 0,
	                              Style = this.FindResource("AddedPointsStyle") as Style,
	                          };
	        this.LayoutRoot.Children.Add(block);

	        var story = new Storyboard();
	        story.Children.Add(new DoubleAnimation(0, 1, TimeSpan.FromMilliseconds(500), FillBehavior.HoldEnd) {EasingFunction = new CircleEase() {EasingMode = EasingMode.EaseOut}});
	        story.Children.Add(new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(1000), FillBehavior.HoldEnd) {BeginTime = TimeSpan.FromSeconds(2), EasingFunction = new CircleEase() {EasingMode = EasingMode.EaseOut}});
	        Storyboard.SetTarget(story.Children[0], block);
	        Storyboard.SetTarget(story.Children[1], block);
	        Storyboard.SetTargetProperty(story.Children[0], new PropertyPath(OpacityProperty));
	        Storyboard.SetTargetProperty(story.Children[1], new PropertyPath(OpacityProperty));
	        story.Completed += (s, em) => this.LayoutRoot.Children.Remove(block);
	        story.Begin();
	    
	    }
	}
}