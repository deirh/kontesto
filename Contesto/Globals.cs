﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contesto.Logic;

namespace Contesto
{
    internal class Globals
    {
        internal static Overseer Overseer { get; set; }
        internal static GameState GameState { get; set; }

        internal static Uri DataFilePath = new Uri("Data\\GameState.xaml", UriKind.Relative);
    }
}
