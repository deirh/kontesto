﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Contesto
{
    public static class FrameworkElementEx
    {
        public static bool BeginStoryboard(this FrameworkElement element, string storyboardName)
        {
            var story = element.TryFindResource(storyboardName) as Storyboard;
            if (story != null)
                story.Begin();
            return story != null;
        }

        public static bool ResetStoryboard(this FrameworkElement element, string storyboardName)
        {
            var story = element.TryFindResource(storyboardName) as Storyboard;
            if (story != null)
                story.Remove();
            return story != null;
        }
    }

    public static class WindowEx
    {
        public static void ToFullscreen(this Window window, bool useSecondaryScreen)
        {
            if (useSecondaryScreen)
                window.ToSecondaryMonitor();

            window.WindowStyle = WindowStyle.None;
            window.ResizeMode = ResizeMode.NoResize;
            window.WindowStartupLocation = WindowStartupLocation.Manual;
            window.WindowState = WindowState.Maximized;
        }

        public static void ToResizable(this Window window)
        {
            window.WindowStyle = WindowStyle.SingleBorderWindow;
            window.ResizeMode = ResizeMode.CanResizeWithGrip;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.WindowState = WindowState.Normal;
        }

        public static void ToggleFullscreen(this Window window)
        {
            if (window.WindowStyle == WindowStyle.None)
                window.ToResizable();
            else
                window.ToFullscreen(false);
        }

        public static void ToSecondaryMonitor(this Window window)
        {
            if (Screen.AllScreens.Any(sc => !sc.Primary))
            {
                Screen screen = Screen.AllScreens.First(sc => !sc.Primary);
                window.Left = screen.WorkingArea.Left;
                window.Top = screen.WorkingArea.Top;
            }
        }
    }

    public static class VisualTreeEx
    {
        public static IEnumerable<FrameworkElement> EnumerateDescendants(FrameworkElement element)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); ++i)
            {
                var child = VisualTreeHelper.GetChild(element, i) as FrameworkElement;
                if (child != null)
                {
                    yield return child;
                    foreach (var descendant in EnumerateDescendants(child))
                        yield return descendant;
                }
            }
        }
    }
}
