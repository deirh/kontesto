﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Contesto.Logic;
using Contesto.Visual;
using Contesto.Visual.Windows;
using PostSharp.Patterns.Model;

namespace Contesto
{
    public class Overseer
    {
        public OverseerContext Context { get; private set; }

        internal ControlWindow ControlWindow { get; private set; }
        internal PresenterWindow PresenterWindow { get; private set; }

        public IEnumerable<PresentationControl> Presentations
        {
            get
            {
                yield return this.ControlWindow.Presentation;
                yield return this.PresenterWindow.Presentation;
            }
        }

        public Overseer(ControlWindow control, PresenterWindow presenter)
        {
            this.ControlWindow = control;
            this.PresenterWindow = presenter;

            foreach (var presentation in Presentations)
                presentation.Overseer = this;

            this.Context = new OverseerContext();
        }

        public void Begin()
        {
            this.PresenterWindow.DataContext = Globals.GameState;
            this.ControlWindow.DataContext = this.Context;

            this.Context.PresenterShown = false;
            this.PresenterWindow.ToSecondaryMonitor();
            //this.PresenterWindow.Show();
            this.PresenterWindow.Hide();
            
            this.ControlWindow.Show();

        }

        public void TogglePresenterVisibility()
        {
            if (this.PresenterWindow.IsVisible)
                this.PresenterWindow.Hide();
            else
            {
                this.PresenterWindow.Show();
                this.PresenterWindow.ToFullscreen(true);
            }
            this.Context.PresenterShown = this.PresenterWindow.IsVisible;
        }

        public void ChangeScreen(GameScreen newScreen)
        {
            this.Presentations.Multicast( p => p.SwitchScreen(newScreen));
        }
    }

    [NotifyPropertyChanged]
    public class OverseerContext
    {
        public bool PresenterShown { get; set; }
        [IgnoreAutoChangeNotification]
        public GameState GameState { get { return Globals.GameState; } }
    }
}
