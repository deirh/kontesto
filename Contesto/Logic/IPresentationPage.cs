﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Contesto.Logic
{
    public interface IPresentationPage
    {
        GameScreen PageName { get; }
        
        ListView PlayersView { get; }
    }
}
