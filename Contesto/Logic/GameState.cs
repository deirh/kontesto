﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Serialization;
using PostSharp.Patterns.Model;

namespace Contesto.Logic
{
    public enum GameScreen
    {
        Risk,
        Briefcase,
        Results
    }

    [NotifyPropertyChanged]
    [Serializable]
    public class GameState
    {
        public ObservableCollection<Player> Players { get; private set; }
        public ObservableCollection<Player> Mentors { get; private set; }

        public List<Genre> Genres { get; private set; }
        public List<Question> Questions { get; private set; }
        public List<Word> Words { get; private set; }

        public Question QuestionSelected { get; set; }
        public AddPointsRequest AddPointsRequest { get; set; }
        public GameScreen CurrentScreen { get; set; }

        public Word WordSelected { get; set; }
        public int WordCombo { get; set; }
        public int WordTimeMaximumSeconds { get; set; }
        public TimeSpan WordTimeRemaining { get; set; }

        [XmlIgnore]
        public IEnumerator<Word> WordEnumerator;

        public GameState()
        {
            this.Players = new ObservableCollection<Player>();
            this.Mentors = new ObservableCollection<Player>();
            this.Genres = new List<Genre>();
            this.Questions = new List<Question>();
            this.Words = new List<Word>();
            
            this.AddPointsRequest = new AddPointsRequest();
            this.CurrentScreen = GameScreen.Risk;
        }

        #region Save / Load / Validate

        public static GameState Load(Uri path)
        {
            var statePath = new Uri(path + ".state", path.IsAbsoluteUri ? UriKind.Absolute : UriKind.Relative);
            if (File.Exists(statePath.OriginalString))
            {
                try
                {
                    // Load running state file
                    using (var dataStream = File.OpenRead(statePath.OriginalString))
                    {
                        var serialKiller = new XmlSerializer(typeof (GameState));
                        GameState gameState = serialKiller.Deserialize(dataStream) as GameState;
                        if (!gameState.Validate())
                            throw new Exception("GameState validation failed.");
                        return gameState;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Application.Current.MainWindow, "GameState loading failed, proceeding to try new game template." + Environment.NewLine + "Reason: " + ex.Message, "Error loading data");
                }
            }

            // Load new game template
            using (var dataStream = File.OpenRead(path.OriginalString))
            {
                GameState gameState = XamlReader.Load(dataStream) as GameState;
                if (gameState == null || !gameState.Validate())
                {
                    MessageBox.Show(Application.Current.MainWindow, "Failed to load new game template. Numbers of items do not match.", "Error loading data");
                    throw new InvalidDataException("Failed to load game state");
                }
                return gameState;
            }
        }

        public bool Save(Uri path)
        {
            try
            {
                path = new Uri(path + ".state", path.IsAbsoluteUri ? UriKind.Absolute : UriKind.Relative);
                File.Delete(path.OriginalString);
                var settings = new XmlWriterSettings 
                {
                    Indent = true
                };
                using (var dataStream = XmlWriter.Create(File.OpenWrite(path.OriginalString), settings))
                {
                    var what = new XmlSerializer(typeof (GameState));
                    what.Serialize(dataStream, this);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save data into: " + path + Environment.NewLine + "Reason: " + ex.Message, "Error saving data");
                return false;
            }
        }

        protected bool Validate()
        {
            this.WordEnumerator = this.Words.GetEnumerator();
            this.WordEnumerator.MoveNext();
            this.WordTimeRemaining = TimeSpan.FromSeconds(this.WordTimeMaximumSeconds);

            if (this.WordSelected != null)
            {
                while (this.WordEnumerator.Current.Text != this.WordSelected.Text)
                {
                    if (!this.WordEnumerator.MoveNext())
                        return false;
                }
            }
            return this.Players != null && this.Players.Count == 3
                   && this.Genres != null && this.Genres.Count == 4
                   && this.Questions != null && this.Questions.Count == this.Genres.Count * 4;
        }

        #endregion

        public void AddPointsRequested()
        {
            Player player = this.Players.FirstOrDefault(p => p == this.AddPointsRequest.Player);
            if (player != null)
                player.Score += this.AddPointsRequest.Amount;
        }


    }

    [NotifyPropertyChanged]
    public class Player
    {
        public string Name { get; set; }
        public int Score { get; set; }
    }

    public class Genre
    {
        public string Name { get; set; }
        public int Order { get; set; }
    }

    [NotifyPropertyChanged]
    public class Question
    {
        public string Text { get; set; }
        public string ImagePath { get; set; }
        public Genre Genre { get; set; }
        public int Value { get; set; }
        public bool IsExpended { get; set; }
    }

    public class Word
    {
        public string Text { get; set; }
    }

    [NotifyPropertyChanged]
    public class AddPointsRequest
    {
        public Player Player { get; set; }
        public int Amount { get; set; }
    }
}
