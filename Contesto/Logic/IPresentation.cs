﻿namespace Contesto.Logic
{
    public interface IPresentation
    {
        IPresentationPage CurrentPage { get; }

        void SwitchScreen(GameScreen newScreen);
        void AnimatePointsAdded();
    }
}
