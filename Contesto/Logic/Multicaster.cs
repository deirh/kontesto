﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contesto.Logic
{
    public static class Multicaster
    {
        public static void Multicast<TInterface>(this IEnumerable<TInterface> targets, Expression<Action<TInterface>> actionExpression)
        {
            // Lambda
            // Expressions
            // Fuckin
            // RULE

            var action = actionExpression.Compile();
            foreach (TInterface target in targets)
            {
                action(target);
            }
        }
    }
}
